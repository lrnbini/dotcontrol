﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotControl
{
    public interface ISettingsProvider
    {
        double GetAccountBalanceMultiplier();
    }
}
