﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DotControl
{
    class Program
    {
        static void Main(string[] args)
        {
            Guid ID = Guid.NewGuid();
            double currentBalance = 3300.00;
            double balanceMultiplier = 3.1;
            IAccountBalanceService ABS = new AccountBalanceService(ID, currentBalance);
            ISettingsProvider SP = new SettingsProvider(balanceMultiplier);
            Account account = new Account(ID, ABS, SP);
            Thread thread;
            thread = new Thread(new ThreadStart(account.RefreshBalance));
            thread.Start();
            thread.Join();
            Console.WriteLine("Balance is {0}", account.Balance);
        }
    }


    
   
}
