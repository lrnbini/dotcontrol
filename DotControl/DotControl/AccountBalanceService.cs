﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DotControl
{
    public class AccountBalanceService : IAccountBalanceService
    {
        private double _currentAccountBalance;
        private Guid _id;
        private object token = new object();

        public AccountBalanceService(Guid AccountId, double AccountBalance)
        {
            _id = AccountId;
            _currentAccountBalance = AccountBalance;
        }


        public double GetCurrentAccountBalance(Guid accountId)
        {
            lock (token)
            {
                if (this._id == accountId)
                {
                    return _currentAccountBalance;
                }
                else
                {
                    return -1;
                }
            }
            
        }


    }
}
