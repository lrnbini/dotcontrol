﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotControl
{
    public class SettingsProvider : ISettingsProvider
    {
        private double AccountBalanceMultiplier;

        public SettingsProvider(double ABM)
        {
            this.AccountBalanceMultiplier = ABM;
        }

        public double GetAccountBalanceMultiplier()
        {
            return AccountBalanceMultiplier;
        }
    }
}
