﻿using System;
using System.Threading;

namespace DotControl
{
    public class Account
    {
        private readonly Guid _accountId;
        private readonly IAccountBalanceService _accountBalanceService;
        private readonly ISettingsProvider _settingsProvider;
        public double Balance { get; private set;}


        public Account(Guid accountId,
                       IAccountBalanceService accountBalanceService,
                       ISettingsProvider settingsProvider)
        {
            _accountId = accountId;
            _accountBalanceService = accountBalanceService;
            _settingsProvider = settingsProvider;
        }


        public void RefreshBalance()
        {
            Balance = _accountBalanceService.GetCurrentAccountBalance(_accountId)
                      * _settingsProvider.GetAccountBalanceMultiplier();
        }
    }

}
