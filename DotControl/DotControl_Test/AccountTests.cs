﻿using System;
using DotControl;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DotControl_Test
{
    [TestClass]
    public class AccountTests
    {
        [TestMethod]
        public void RefreshBalance_Test()
        {
            //Arrange
            Guid ID = Guid.NewGuid();
            double currentBalance = 3300.00;
            double balanceMultiplier = 3.1;
            IAccountBalanceService ABS = new AccountBalanceService(ID, currentBalance);
            ISettingsProvider SP = new SettingsProvider(balanceMultiplier);
            Account account = new Account(ID, ABS, SP);
            Thread thread;

            double expected = currentBalance * balanceMultiplier;

            //Act 
            thread = new Thread(new ThreadStart(account.RefreshBalance));
            thread.Start();
            thread.Join();

            //Assert
            double actual = account.Balance;
            Assert.AreEqual(expected, actual);

        }


        [TestMethod]
        public void RefreshBalance__differentID_Test()
        {
            //Arrange
            Guid ID_1 = Guid.NewGuid();
            Guid ID_2 = Guid.NewGuid();
            double currentBalance = 3300.00;
            double balanceMultiplier = 3.1;
            IAccountBalanceService ABS = new AccountBalanceService(ID_1, currentBalance);
            ISettingsProvider SP = new SettingsProvider(balanceMultiplier);
            Account account = new Account(ID_2, ABS, SP);
            Thread thread;

            double expected = -1 * balanceMultiplier;

            //Act 
            thread = new Thread(new ThreadStart(account.RefreshBalance));
            thread.Start();
            thread.Join();

            //Assert
            double actual = account.Balance;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void RefreshBalance__MultiThread_Test()
        {
            //Arrange
            Guid ID = Guid.NewGuid();
            double currentBalance = 3300.00;
            double balanceMultiplier = 3.1;
            IAccountBalanceService ABS = new AccountBalanceService(ID, currentBalance);
            ISettingsProvider SP = new SettingsProvider(balanceMultiplier);
            Account account = new Account(ID, ABS, SP);
            Thread[] threads = new Thread[10];

            //Act
            for (int i = 0; i < 10; i++)
            {
                threads[i] = new Thread(new ThreadStart(account.RefreshBalance));
            }

            foreach (Thread t in threads)
                t.Start();
        }
    }
}
