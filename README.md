DotControl - Lorenzo Bini - Application Assignment for .NET Developers
**********************************************************************

## INTRODUCTION ----

Two classes, apart from the Program.cs class, have been created to implement both the interfaces given with the assignment. 
The AccountBalanceService() class is composed of a redefined constructor which initializes the fields, and the method required by the interface. The field "ID", which type is Guid,
has been included considering that a similar Guid parameter is passed with every call of the main method GetCurrentAccountBalance(). Its implementation is not relevant in this case,
but it would be if the real implementation involves a server call to retrieve the actual current balance, as supposed in the Objective 2 of the assignment.

No changes have been made to the Account() class. The field "Balance", though, should be initialized in the constructor, maybe with a first call at the GetCurrentAccountBalance()
method. 

In this implementation, the currentBalance returned by the AccountBalanceService() class is previously defined, initialized and not modified during execution. 

## OBJECTIVE 1 ----

The GetCurrentAccountBalance() method checks if the account ID passed with the call is equal to the one stored, if so it returns the currentBalance, otherwise it returns -1. 
Two unit test cases have been set up to check the behavior of the method in both cases: when the IDs correspond to each other, and when it doesn't happen.

At first, threads were not used to check the behavior, they were used simple calls instead.

## OBJECTIVE 2 ----

To implement an asynchronous access to the GetCurrentAccountBalance() method for the reasons explained in the assignment text, a lock statement was used to guarantee a single
access to the method at a time. This to prevent concurrency given by multiple threads calling the RefreshBalance() method concurrently. 

Slow responses from ipotetical servers, as supposed in the assignment text, have not been simulated in this implementation.

From this point, another unit test case with multiple threads has been used to test the new implementation. The other two unit test cases has been modified to execute actions 
via thread.

